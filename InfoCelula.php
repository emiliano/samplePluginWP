<?php  # -*- coding: utf-8 -*-
/**
 * Plugin Name: InfoCelula
* Plugin URI: http://emilianolanda.com.mx
* Description: Este plugin muestra la información guardada en la bd de mongo en el server MEAN
* Version: 1.0.0.1
* Author: Emiliano Landa
* Author URI: http://emilianolanda.com.mx 
* License: GPL2
 */

//https://github.com/toscho/T5-Admin-Menu-Demo

/* call our code on admin pages only, not on front end requests or during
 * AJAX calls.
 * Always wait for the last possible hook to start your code.
 */
add_action( 'admin_menu', array ( 'T5_Admin_Page_Demo', 'admin_menu' ) );
/**
 * Register three admin pages and add a stylesheet and a javascript to two of
 * them only.
 *
 * @author toscho
 *
 */
class T5_Admin_Page_Demo
{
	/**
	 * Register the pages and the style and script loader callbacks.
	 *
	 * @wp-hook admin_menu
	 * @return  void
	 */
	public static function admin_menu()
	{
		// $main is now a slug named "toplevel_page_t5-demo"
		// built with get_plugin_page_hookname( $menu_slug, '' )
		$main = add_menu_page(
			'Panel Celula',                         // page title
			'Info Celula',                         // menu title
			// Change the capability to make the pages visible for other users.
			// See http://codex.wordpress.org/Roles_and_Capabilities
			'manage_options',                  // capability
			'panel-celula',                         // menu slug
			array ( __CLASS__, 'render_main_panel' ) // callback function
		);
		// $sub is now a slug named "t5-demo_page_t5-demo-sub"
		// built with get_plugin_page_hookname( $menu_slug, $parent_slug)
		$sub = add_submenu_page(
			'panel-celula',                         // parent slug
			'Resumen',                     // page title
			'Resumen',                     // menu title
			'manage_options',                  // capability
			'resumen',                     // menu slug
			array ( __CLASS__, 'render_resume_panel' ) // callback function, same as above
		);
		/* See http://wordpress.stackexchange.com/a/49994/73 for the difference
		 * to "'admin_enqueue_scripts', $hook_suffix"
		 */
		foreach ( array ( $main, $sub ) as $slug )
		{
			// make sure the style callback is used on our page only
			add_action(
				"admin_print_styles-$slug",
				array ( __CLASS__, 'enqueue_style' )
			);
			// make sure the script callback is used on our page only
			add_action(
				"admin_print_scripts-$slug",
				array ( __CLASS__, 'enqueue_script' )
			);
		}
		// $text is now a slug named "t5-demo_page_t5-text-included"
		// built with get_plugin_page_hookname( $menu_slug, $parent_slug)
		$text = add_submenu_page(
			'panel-celula',                         // parent slug
			'Configuración',                     // page title
			'Configuración',                     // menu title
			'manage_options',                  // capability
			'configuracion',                     // menu slug
			array ( __CLASS__, 'render_settings_panel' ) // callback function, same as above
		);
	}

	/**
	 * Print page output.
	 *
	 * @wp-hook toplevel_page_t5-demo In wp-admin/admin.php do_action($page_hook).
	 * @wp-hook t5-demo_page_t5-demo-sub
	 * @return  void
	 */
	public static function render_main_panel()
	{
		global $title;
		print '<div class="wrap">';
		print "<h1>$title</h1>";
		$file = plugin_dir_path( __FILE__ ) . "/html/info-celula.html";
		if ( file_exists( $file ) )
			require $file;
		//self::sample_function();
		print '</div>';
	}

	/**
	 * Print included HTML file.
	 *
	 * @wp-hook t5-demo_page_t5-text-included
	 * @return  void
	 */
	public static function render_resume_panel()
	{
		global $title;
		print '<div class="wrap">';
		print "<h1>$title</h1>";
		$file = plugin_dir_path( __FILE__ ) . "/html/resumen.html";
		if ( file_exists( $file ) )
			require $file;
		print "<p class='description'>Included from <code>$file</code></p>";
		print '</div>';
	}

	public static function render_settings_panel()
	{
		global $title;
		print '<div class="wrap">';
		print "<h1>$title</h1>";
		$file = plugin_dir_path( __FILE__ ) . "/html/settings.html";
		if ( file_exists( $file ) )
			require $file;
		print "<p class='description'>Included from <code>$file</code></p>";
		print '</div>';
	}

	/**
	 * Load stylesheet on our admin page only.
	 *
	 * @return void
	 */
	public static function enqueue_style()
	{
		wp_register_style(
			'info_panel_css',
			plugins_url( 'info-panel.css', __FILE__ )
		);
		wp_enqueue_style( 'info_panel_css' );
	}

	/**
	 * Load JavaScript on our admin page only.
	 *
	 * @return void
	 */
	public static function enqueue_script()
	{
		wp_register_script('jquery', 
			"http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . '://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js', 
			array(),
			FALSE,
			TRUE
		);
		wp_enqueue_script('jQuery');

		wp_register_script('gstatic', 
			'https://www.gstatic.com/charts/loader.js', 
			array(),
			FALSE,
			TRUE
		);
		wp_enqueue_script('gstatic');

		wp_register_script(
			'info_panel_js',
			plugins_url( 'info-panel.js', __FILE__ ),
			array(),
			FALSE,
			TRUE
		);
		wp_enqueue_script( 'info_panel_js' );
	}

	/**
	 * List available global variables.
	 *
	 * @return void
	 */
	protected static function sample_function()
	{
		
	}
	
}