j = jQuery.noConflict();

function settingGCharts(){
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);
}

function drawChart(datas) {
	// Create the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Topping');
	data.addColumn('number', 'Slices');
	data.addRows([
	  ['Mushrooms', 3],
	  ['Onions', 1],
	  ['Olives', 1],
	  ['Zucchini', 1],
	  ['Pepperoni', 2]
	]);
	// Set chart options
	var options = {'title':'How Much Pizza I Ate Last Night',
	               'width':400,
	               'height':300};

	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
	chart.draw(data, options);
}

function bindControlsUI(){
	j('.test_anchor').click(function(){
		console.log('click en el link');
	});


}

function initJqueryFunction(){
	console.log('Hola :D' );
	bindControlsUI();
	settingGCharts();
}
j( document ).ready(initJqueryFunction);

